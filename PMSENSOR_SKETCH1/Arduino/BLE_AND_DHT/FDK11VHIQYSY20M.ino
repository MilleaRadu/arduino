/*
 * CONNECT TO OUTGOING PORT HC-05 'DEV-B' TO VIEW DATA ON SERIAL MONITOR
 * USE THIS SKETCH ONLY FOR VIEWING SENSOR DATA ON SERIAL MONITOR.....NOT FOR FILE WRITING
 */

#include <DHT.h>
#include <DHT_U.h>
#include <SoftwareSerial.h>

SoftwareSerial BTSerial(11,10);

#include <Adafruit_Sensor.h>

#include <Adafruit_BMP183_U.h>

#include <DHT.h>


#define DHTTYPE DHT22
#define DHTPIN 2


DHT dht(DHTPIN,DHTTYPE);
 
int temp; //variable to hold temperature sensor value
long tm,t,d; //variables to record time in seconds

void setup()
{
  Serial.begin(9600);
  BTSerial.begin(9600);
  dht.begin();
 // pinMode(0,INPUT);//temperature sensor connected to analog 0
  //analogReference(DEFAULT);
}

void loop()
{
  //temp = analogRead(0); //analog reading temperature sensor values
   float temp = dht.readTemperature();
    float hum = dht.readHumidity();//read dht22 temp and humidity;
  //required for converting time to seconds
 /// tm = millis();
  //t = tm/1000;
 /// d = tm%1000;

 // BTSerial.flush();

  //printing time in seconds
//  BTSerial.print("time : ");
//  BTSerial.print(t);
//  BTSerial.print(".");
//  BTSerial.print(d);
//  BTSerial.print("s\t");

  //printing temperature sensor values
//   BTSerial.print("Temperture : ");
    BTSerial.print(temp);
    //BTSerial.print(" C  and Humidity : ");
    BTSerial.print(hum);
   // BTSerial.println("----------------");
   // Serial.print(temp);
  
  delay(2000);//delay of 2 seconds
  //before upload disconnect rx andd tx pins from ardino board ;
  //after upload connect back
}
