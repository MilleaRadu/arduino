/*
 * CONNECT TO OUTGOING PORT HC-05 'DEV-B' TO VIEW DATA ON SERIAL MONITOR
 * USE THIS SKETCH ONLY FOR VIEWING SENSOR DATA ON SERIAL MONITOR.....NOT FOR FILE WRITING
 */

#include <DHT.h>
#include <DHT_U.h>
#include <SoftwareSerial.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP183_U.h>
#include <DHT.h>


#define DHTTYPE DHT22
#define DHTPIN 2
#define USE_AVG//to display avg of dust value

SoftwareSerial BTSerial(11,10);
DHT dht(DHTPIN,DHTTYPE);
 
int temp; //variable to hold temperature sensor value
long tm,t,d; //variables to record time in seconds
const int sharpLEDPin =7; //turn HIGH to turn LED off and turn LOW to turn LET on;
const int sharpVoPin=A5;//connect for reading
static float Voc=0.6; //output voltage when no dust;
const float K = 0.3; //sensitivity in units of V per 100/m3;

#ifdef USE_AVG
#define N 200
static unsigned long VoRawTotal=0;
static int VoRawCount=0;
#endif //USE_AVG

void setup()
{
  pinMode(sharpLEDPin,OUTPUT);
  Serial.begin(9600);
  BTSerial.begin(9600);
  dht.begin();
  delay(2000);
 // pinMode(0,INPUT);//temperature sensor connected to analog 0
  //analogReference(DEFAULT);
}

void loop() {
  // put your main code here, to run repeatedly:
    digitalWrite(sharpLEDPin,LOW);//turn on led
    delayMicroseconds(280);
    int VoRaw=analogRead(sharpVoPin);//take reading
    digitalWrite(sharpLEDPin,HIGH);//turn off led
     delayMicroseconds(9620);
     float Vo = VoRaw;
  #ifdef USE_AVG
  VoRawTotal += VoRaw;
  VoRawCount++;
  if ( VoRawCount >= N ) {
    Vo = 1.0 * VoRawTotal / N;
    Vo=Vo/1024.0*5.0;
  float dV = Vo - Voc;
  if ( dV < 0 ) {
    dV = 0;
    Voc = Vo;
  }
  float dustDensity = dV / K * 100.0;
 //// Serial.print(VoRawCount);
 Serial.println("  reached 100 measures : ");
  Serial.println(dustDensity);
  
    VoRawCount = 0;
    VoRawTotal = 0;
  } else {
//    return;
  }
  #endif // USE_AVG
  //Serial.println("nothing");
//  Vo=Vo/1024.0*5.0;
//  float dV = Vo - Voc;
//  if ( dV < 0 ) {
//    dV = 0;
//    Voc = Vo;
//  }
//  float dustDensity = dV / K * 100.0;
// //// Serial.print(VoRawCount);
//  Serial.println(dustDensity);
 
    float temp = dht.readTemperature();
    float hum = dht.readHumidity();
    if(BTSerial.read()>0){
     // char a = Serial.read();
      //switch(a){
       // case 'a': BTSerial.print(temp);
                  BTSerial.print(temp);
                  BTSerial.print(" ");
               //   BTSerial.print(dustDensity);
                  BTSerial.print(" ");
                  BTSerial.print(hum);
                  Serial.print(temp);
                  Serial.print("  ");
             //     Serial.print(dustDensity);
                  
                //  break;
        //case 'd' : BTSerial.print("salut");break;
        //default: break;
     // }
    }else{
      //delay(2000);
    }
}
