#include <DHT.h>
#include <DHT_U.h>

#include <Adafruit_Sensor.h>

#include <Adafruit_BMP183_U.h>

#include <DHT.h>


#define DHTTYPE DHT22
#define DHTPIN 2


DHT dht(DHTPIN,DHTTYPE);

void setup() {
  Serial.begin(9600);
  
  dht.begin();

}

void loop() {
  // put your main code here, to run repeatedly:
    float temp = dht.readTemperature();
    float hum = dht.readHumidity();

    Serial.print("Temperture : ");
    Serial.print(temp);
    Serial.print(" C  and Humidity : ");
    Serial.print(hum);
    Serial.println("----------------");

    delay(15000);
    
}
